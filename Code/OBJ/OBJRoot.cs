﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Evryway.ColladaExporter
{
    public class OBJRoot
    {
        public string path { get; private set; }
        public string path_materials { get; private set; }
        public System.Text.StringBuilder sb { get; private set; }
        public List<Material> materials { get; private set; }
        public List<Mesh> meshes { get; private set; }
        public GameObject root { get; private set; }
        public List<string> texturePaths { get; private set; }

        public Dictionary<Material, string> matTexLookup { get; private set; }
        public Dictionary<Mesh, Material> meshMatLookup { get; private set; }
        public Dictionary<MeshFilter, Material> mfMatLookup { get; private set; }
        public Dictionary<Mesh, MeshFilter> meshMfLookup { get; private set; }
        public Dictionary<Mesh, int> meshStartIndices { get; private set; }

        public OBJRoot(string path)
        {
            this.path = path;
            this.path_materials = path.Substring(0,path.Length - 3) + "mtl";
            this.sb = new System.Text.StringBuilder();
            sb.EnsureCapacity(50 * 1024 * 1024);            // 50MB should be enough for pretty much any model. more than this and I'm screwed.
        }


        public void Process(GameObject go, List<string> texturePaths)
        {
            Prepare(go, texturePaths);
            CreateMaterialsFile();
            CreateGeometryFile();
        }

        public void Prepare(GameObject go, List<string> texturePaths)
        {
            // I've got a game object, which should have a selection of child objects.
            // each of those should be considered one group for material purposes.
            root = go;
            this.texturePaths = texturePaths;

            // work with a flat list of materials while setting up the material / effects / images libraries.
            materials = root.GetComponentsInChildren<MeshRenderer>().Select(mr => mr.sharedMaterial).ToList();
            // this SHOULD be selectMANY if we want to support multi-material meshes.

            var meshfilters = root.GetComponentsInChildren<MeshFilter>().ToList();

            meshes = meshfilters.Select( mf => mf.mesh).ToList();

            // match up the materials to the relevant textures.
            matTexLookup = new Dictionary<Material, string>();

            foreach (var mat in materials)
            {

                var expected = string.Format("textures/{0}_maintex", mat.name);
                var found = texturePaths.Find(q => q.Contains(expected));
                if (found != null) expected = found;
                else expected = expected + ".jpg";

                string tp = string.Format("file://{0}", expected);

                // but actually, I do NOT want the full path at all, because that's giving me a device path.
                // I just want a path rooted at textures.
                var tps = tp.IndexOf("textures/");
                tp = tp.Substring(tps);

                matTexLookup[mat] = tp;
            }

            // EACH MESH SHOULD HAVE ONE MATERIAL. UNTIL I SUPPORT MULTIMATERIALS. WHICH I'M NOT RIGHT NOW.
            meshMatLookup = new Dictionary<Mesh, Material>();
            mfMatLookup = new Dictionary<MeshFilter, Material>();
            meshMfLookup = new Dictionary<Mesh, MeshFilter>();
            foreach (var mf in meshfilters)
            {
                var mesh = mf.mesh;
                var mat = mf.gameObject.GetComponent<MeshRenderer>().sharedMaterial;
                meshMatLookup[mesh] = mat;
                mfMatLookup[mf] = mat;
                meshMfLookup[mesh] = mf;
            }

            meshStartIndices = new Dictionary<Mesh, int>();

            // prepare the start indices.
            int v_idx = 0;
            foreach (var mesh in meshes)
            {
                meshStartIndices[mesh] = v_idx;
                v_idx += mesh.vertexCount;
            }


        }

        public void CreateMaterialsFile()
        {

            // need a text file.
            sb.Length = 0;
            sb.Append("# Evryway Scanner material file\n");
            sb.AppendFormat("# contains {0} materials\n",materials.Count);
            foreach (var mat in materials)
            {
                AppendMaterial(mat);
            }

            // we've got it.
            var output = sb.ToString();

            //Debug.LogFormat(output);
            System.IO.File.WriteAllText(path_materials, output);
        }

        // https://en.wikipedia.org/wiki/Wavefront_.obj_file
        public void AppendMaterial(Material mat)
        {
            sb.AppendFormat("# - - - {0}\n",mat.name);
            sb.AppendFormat("newmtl {0}\n",mat.name);
            sb.Append("Ka 1.0 1.0 1.0\n");                // AMBIENT - white
            sb.Append("Kd 1.0 1.0 1.0\n");                // DIFFUSE - white
            sb.Append("illum 1\n");                       // ILLUMINATION MODE (ambient on, diffuse on)

            var mat_path = matTexLookup[mat];

            sb.AppendFormat("map_Kd {0}\n", mat_path);
        }



        public void GeometryFileHeader()
        {
            int total_vertices = meshes.Sum(mesh => mesh.vertexCount);
            int total_triangles = meshes.Sum(mesh => mesh.triangles.Length);

            var objname = System.IO.Path.GetFileNameWithoutExtension(path);

            sb.Length = 0;
            sb.Append("# Created with Evryway Scanner\n");
            sb.AppendFormat("# contains {0} vertices, {1} triangles, {2} meshes\n", total_vertices, total_triangles, meshes.Count);
            sb.AppendFormat("o EvrywayScanner_{0}\n", objname);

            // put the material library in
            sb.AppendFormat("mtllib {0}\n", System.IO.Path.GetFileName(path_materials));

            // set up all the mesh 
        }

        public void GeometryFileVertices()
        {
            // write all vertices
            GeometryFileVerticesHeader();
            foreach (var m in meshes) GeometryFileVerticesMesh(m);
        }

        public void GeometryFileVerticesHeader()
        {
            sb.Append("#--- Vertices\n");
        }

        public void GeometryFileVerticesMesh(Mesh mesh)
        {
            var verts = mesh.vertices;
            foreach (var v in verts) sb.AppendFormat("v {0} {1} {2}\n", -v.x, v.y, v.z);
        }

        public void GeometryFileNormals()
        {
            // write all normals
            GeometryFileNormalsHeader();
            foreach (var m in meshes) GeometryFileNormalsMesh(m);
        }

        public void GeometryFileNormalsHeader()
        {
            sb.Append("#--- normals\n");
        }

        public void GeometryFileNormalsMesh(Mesh mesh)
        {
            var norms = mesh.normals;
            foreach (var n in norms)
            {
                sb.AppendFormat("vn {0} {1} {2}\n", -n.x, n.y, n.z);
            }
        }

        public void GeometryFileUVs()
        {
            // write all UVs
            GeometryFileUVsHeader();
            foreach (var m in meshes) GeometryFileUVsMesh(m);
        }
        public void GeometryFileUVsHeader()
        {
            sb.Append("#--- UVS\n");
        }
        public void GeometryFileUVsMesh(Mesh mesh)
        {
            var uvs = mesh.uv;
            foreach (var uv in uvs)
            {
                sb.AppendFormat("vt {0} {1}\n", uv.x, uv.y);
            }

        }

        public void GeometryFileTriangles()
        {
            // write all triangles
            GeometryFileTrianglesHeader();
            foreach (var m in meshes) GeometryFileTrianglesMesh(m);
        }

        public void GeometryFileTrianglesHeader()
        {
            sb.Append("#--- Triangles\n");
        }

        public void GeometryFileTrianglesMesh(Mesh mesh)
        {

            // write each group for each mesh.
            // each mesh should be in a separate group with a separate material.

            var mat = meshMatLookup[mesh];
            var mf = meshMfLookup[mesh];

            sb.AppendFormat("# MESH GROUP {0}\n", mf.name);
            sb.AppendFormat("g {0}\n", mf.name);
            sb.AppendFormat("usemtl {0}\n", mat.name);

            var ts = mesh.triangles;
            var offset = meshStartIndices[mesh] + 1;           // OBJ format indices start at 1, not 0.
            for (int i = 0; i < ts.Length; i += 3)
            {
                var a = ts[i] + offset;
                var b = ts[i + 2] + offset;
                var c = ts[i + 1] + offset;
                sb.AppendFormat("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n", a, b, c);
            }
        }


        // this is the LONG BIT.
        public void CreateGeometryFile()
        {
            GeometryFileHeader();

            GeometryFileVertices();
            GeometryFileNormals();
            GeometryFileUVs();
            GeometryFileTriangles();
        }



        public void Save()
        {
            System.IO.File.WriteAllText(path, sb.ToString());

            // clear out the string builder! it's a LOT of memory hanging around.
            sb.Length = 0;
        }

    }
}
