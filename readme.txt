Evryway Collada Exporter

(c) 2016 Tim Swan @Evryway

See licence.txt for sharing information.


What's this?

Some COLLADA export code which allows runtime exporting of meshes and textures
to a single output archive file (.zip) containing a COLLADA .DAE file and
a directory of textures.

For more details on the COLLADA format, look here:

https://www.khronos.org/collada/
https://www.khronos.org/files/collada_reference_card_1_4.pdf




Installing:

Grab the code, put it somewhere in Unity's Assets directory.
You will need DotNetZip for Unity:
https://github.com/r2d2rigo/dotnetzip-for-unity
Grab the relevant DLL and put it in a Plugins folder somewhere.



HowTo:

1. Create a ColladaRoot object, pathed to your output location:

    var path = Application.dataPath + "/Evryway-Core-Assets/Art/Generated/Collada/Test1/cubetest1.dae";
    var collada_root = new ColladaRoot(path);

2. Construct the COLLADA object, optionally passing in a list of paths to known textures

    var prefab = Resources.Load("Prefabs/Debug/RoundTripCube") as GameObject;
    var testGo = GameObject.Instantiate(prefab);

    collada_root.Construct(testGo, null);

3. Save the COLLADA file.

    colllada_root.Save();


Problems? drop me a mail, or try and fix it yourself - send me a pull req
and I'll do what I can to bring any improvements back.


Known issues:

(20160430) exported geometry is offset from the expected transform position/orientation
(20160430) Editor tools don't export textures
(20160430) Many known unknowns


Releases:

V 0.0.1     20160430

First release. Probably won't work for you.



Contributors:

Tim Swan (2016) @Evryway

